# Changelog

## [1.4.2] - 2020-12-29

Small fixes

### Fixed

- Compatible with GM Screen.
- Compatible with 0.7.9

## [1.4.1] - 2020-11-09

To celebrate the first official system in Foundry, OneJournal is now fully
compatible with the WFRP4E style!

### Added

- Complete reskin of OneJournal for the wfrp4e system

### Fixed

- Fix double rendering of Permission Viewer
- Fix 0.7.6 compatibility

## [1.4.0] - 2020-11-06

Finally 0.7.5 support, sorry about the delay

### Added

- Spanish translation thanks to José Lozano

### Changed

- Due to changes to application windows, only version 0.7.5 and newer is
  supported

### Fixed

- A lot of fixes for 0.7.5
- A lot of fixes for PopOut! module
- Swapping between Text/Image in detached windows work as intended

## [1.3.0] - 2020-09-23

MoreThanOneJournal

### Added

- Added the ability to open journal entries in detached windows, outside of
  OneJournal
  - Shift-click a journal entry
  - New option in the OneJournal sidebar context menu
  - Does not work well when switching between Text/Image yet

## [1.2.11] - 2020-08-16

Customization and compatibility

### Added

- Double click anywhere in the journal to edit (disable in settings)
- Change sidebar size (in settings)
- PopOut! compatible
- Compact sidebar (toggle in settings)

## [1.2.10] - 2020-08-16

Version skipped for technical reasons

## [1.2.9] - 2020-07-28

Hotfix

### Fixed

- Avoid an error introduced in the last release

## [1.2.8] - 2020-07-28

Small uppdate, support for Permission Viewer and one fix

### Added

- Compatiblility with Permission Viewer

### Fixed

- Content height/overflow was messed up if "Show folder selection" was turned on

## [1.2.7] - 2020-07-27

Hotfix

### Fixed

- accidentally removed create buttons

## [1.2.6] - 2020-07-26

Mostly bug fixes

### Added

- (enable in settings) Set folder borders to same color as header
- Setting to hide the sidebar completely for players

### Fixed

- Sidebar toggle stopped working if sidebar re-rendered
- Journal sheet would sometimes close if re-rendered, leaving OneJournal in an
  inconsistent state
- Sidebar toggle also available for players who can't create journals

## [1.2.5] - 2020-07-18

Localization update. English localization for all strings, please open an
issue/merge request or contact me to add languages!

### Added

- Localization for all readable strings (buttons, hints, etc.)

### Fixed

- Get placeholder back in place (when no journal is open)
- Minor css fixes

## [1.2.4] - 2020-06-30

Yet another version bump for technical reasons...

## [1.2.3] - 2020-06-30

Another version bump for technical reasons

## [1.2.2] - 2020-06-30

Minor version bump for technical reasons

### Changed

- Improved window flexibility

## [1.2.1] - 2020-06-25

Polishing update.

### Added

- Sidebar collapse state saved per client
- Experimental: Use browser history for navigation

### Changed

- JournalEntry app header is merged into the header of the outer shell.

### Fixed

- Styling fixes.
- Fix flicker when switching between entries.
- Bump compatiblity to Foundry 0.6.4
- Bug fixes

## [1.2.0] - 2020-05-26

Navigation update!

### Added

- Navigate history with context menu
- Collapse sidebar with toggle
- Option to turn off sidebar sync (folder expansion/scrolling)
- Option to show default folder selector (useful w/ sidebar sync off)
- Automatically remove duplicates in history (default on, toggle in settings)

### Fixed

- Somewhat better compatibility with Pin module

## [1.1.0] - 2020-05-25

Settings update! This update adds new settings for OneJournal, as well as bug
fixes and QoL updates.

### Added

- Change the directory sidebar display to Right/Left/None
- Per-client setting to stop OneJournal from managing windows (effectively
  disables OneJournal)
- Optionally add a button to the default journal directory to open OneJournal
  directly.
- GM can turn off OneJournal for all non-GM users
- Clear history with context menu on history arrow buttons

### Changed

- Some refactoring

### Fixed

- History browsing fixed for journals opened from compendium

## [1.0.0] - 2020-05-19

First public release!

### Added

- Focus the currently open journal within the directory.
- History - go back and forth in your journal browsing history.

### Changed

- Minor 0.5.7 compatibility fixes

### Removed

### Fixed

- Minor fixes and refactoring
- Style fixes

## [0.1.0] - 2020-05-15

### Added

- First release of OneJournal
