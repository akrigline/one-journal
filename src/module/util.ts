export const i18n = name => game.i18n.localize(`ONEJOURNAL.${name}`);

export function scrollElementIntoView(
  element: JQuery<HTMLElement>,
  scrollable: JQuery<HTMLElement>
) {
  if (!element.offset()) {
    return;
  }
  const topOffset = scrollable.offset().top;
  const elementTop = element.offset().top - topOffset;
  const elementBottom = elementTop + element.height();

  if (elementTop < 1) {
    // element is above fold
    scrollable.scrollTop(
      element.offset().top - topOffset + scrollable.scrollTop()
    );
  }

  if (elementBottom > scrollable.height()) {
    // element is below fold
    scrollable.scrollTop(
      element.offset().top -
        topOffset +
        scrollable.scrollTop() -
        scrollable.height() +
        element.outerHeight()
    );
  }
}
